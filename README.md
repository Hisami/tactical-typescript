# Description du Projet

C'est un projet de jeu tactique en TypeScript qui se déroule pendant l'ère Sengoku du Japon. Le jeu est conçu pour deux joueurs, et à chaque tour, un joueur peut choisir une unité pour se déplacer et attaquer une fois. La partie se joue en 10 tours, et les conditions de victoire sont les suivantes : soit vaincre le général ennemi, soit, à défaut, avoir plus d'unités survivantes que l'adversaire à la fin des 10 tours.

## Plateau de Jeu

Le plateau de jeu est composé de 7x7 cases. À chaque tour, sélectionnez l'unité souhaitée et cliquez sur la case de destination pour déplacer cette unité.

## Unités

Les différentes unités disponibles dans le jeu sont les suivantes :

- **Le Général** : Il se caractérise par une grande puissance de combat et une grande mobilité.
- **Les Archers** : Ils ont une faible défense mais une portée d'attaque plus étendue.
- **Les Soldats** : Ils ont des statistiques de combat et de défense moyennes.
- **Le Médecin** : Il peut choisir un allié et restaurer partiellement ses points de vie.

Chacune de ces 7 unités possède des caractéristiques uniques qui influencent le gameplay.
