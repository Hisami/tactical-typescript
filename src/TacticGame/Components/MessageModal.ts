
export class MessageModal{
    private container:HTMLDivElement;
    private messageBox:HTMLDivElement;
    private button:HTMLButtonElement;

    constructor(){
        this.container = document.createElement('div');
        this.container.className = 'message-modal';
        this.messageBox = document.createElement('div');
        this.messageBox.className ='message-modalBox';
        this.button = document.createElement("button");
        this.button.className="btn";
        this.button.textContent="Close";
        this.button.addEventListener('click',()=>{
            this.hide();
        })
        this.container.appendChild(this.messageBox);
        this.container.appendChild(this.button);
        document.body.appendChild(this.container);
    }
    public showMessage(message:string){
        this.messageBox.innerText = message;
        this.show();
    }
    public show(){
        this.container.style.display = 'block';
    }
    public hide(){
        this.container.style.display='none';
    }
}