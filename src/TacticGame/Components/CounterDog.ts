
export class CounterDog{
    private container:HTMLDivElement;
    private messageBox:HTMLDivElement;

    constructor(){
        this.container = document.createElement('div');
        this.container.className="message-dog";
        this.messageBox = document.createElement('div');
        this.messageBox.className = "balloon2"
        const p = document.createElement('p');
        
        this.messageBox.appendChild(p);
        const img = document.createElement('img');
        img.setAttribute("src","dog_tosaken.png");
        img.setAttribute("height", "200");
        img.setAttribute("width", "200");
        this.container.appendChild(this.messageBox);
        this.container.appendChild(img);
        document.body.appendChild(this.container);
    }
    
    public changeMessage(message:string){
        this.messageBox.innerText = message;
    }

    public changeMessageandPosition(message:string,player:string){
        this.messageBox.innerText = message;
        if(player=="player1"){
            this.container.className="message-dog";
            this.messageBox.style.backgroundColor= "#ff000065";
        }else{
            this.container.className="message-dog2";
            this.messageBox.style.backgroundColor="#fff";
        }
    }

    public show(){
        this.messageBox.style.display = 'block';
    }
    public hide(){
        this.container.style.display='none';
    }
}