export class MessageWindow{
    private container:HTMLDivElement;
    private messageBox:HTMLDivElement;

    constructor(){
        this.container = document.createElement('div');
        this.container.className = 'message-window';
        this.messageBox = document.createElement('div');
        this.messageBox.className ='message-box';      
        this.container.appendChild(this.messageBox);
        document.body.appendChild(this.container);
    }
    public showMessage(message:string){
        this.messageBox.innerText = message;
        this.show();
    }
    public show(){
        this.container.style.display = 'block';
    }
    public hide(){
        this.container.style.display='none';
    }
}