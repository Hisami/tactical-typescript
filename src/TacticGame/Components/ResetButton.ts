export class ResetButton{
    resetButton:HTMLButtonElement;
    constructor(){
        this.resetButton = document.createElement('button');
        this.resetButton.textContent="Redémarrer le jeu";
        this.resetButton.className="btn2";
        this.resetButton.addEventListener('click',()=>{
            location.reload();
        }) 
    }

    public getButton():HTMLButtonElement{
        return this.resetButton;
    }

}