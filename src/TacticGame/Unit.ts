import { Game } from "./Game";
import { MessageModal } from "./Components/MessageModal";

export class EventUnitDeath extends Event{
    constructor(public unit: Unit) {
        super('unit-death');
    }
}

export class Unit{
    player:string
    name:string;
    moveRange:number;
    attackRange:number;
    action:string;
    attack:number;
    defense:number;
    hp:number;
    positionX:number;
    positionY:number;
    alive:boolean=true;
    messageModal:MessageModal;
    
    constructor(player:string,name:string,moveRange:number,attackRange:number,action:string,attack:number,defense:number, positionX:number,
        positionY:number,hp:number,messageModal:MessageModal){ 
        this.player = player;
        this.moveRange = moveRange;
        this.attackRange = attackRange; 
        this.name = name;
        this.action = action;
        this.attack= attack;
        this.defense = defense;
        this.positionX = positionX;
        this.positionY = positionY;
        this.hp = hp;
        this.messageModal = messageModal; 
    }

    public availableMove(destinationX:number, destinationY:number):boolean{
        if(!this.isMyturn()){
            this.messageModal.showMessage("Ce n'est pas ton tour.")
            return false;
        }
        const distanceX = Math.abs(this.positionX-destinationX);
        const distanceY = Math.abs(this.positionY-destinationY);
        const totalDistance = distanceX + distanceY;
        if(totalDistance <= this.moveRange){
            return true;
        }else{
            return false;
        }
    }
    public availableAction(destinationX:number, destinationY:number):boolean{
        if(!this.isMyturn()){
            this.messageModal.showMessage("Ce n'est pas ton tour.")
            return false;
        }
        const distanceX = Math.abs(this.positionX-destinationX);
        const distanceY = Math.abs(this.positionY-destinationY);
        const totalDistance = distanceX + distanceY;
        if(totalDistance <= this.attackRange){
            return true;
        }else{
            return false;
        }
    }

    public availableActionWithTarget(enemy:Unit):boolean{
        if(!this.isMyturn()){
            this.messageModal.showMessage("Ce n'est pas ton tour.")
            return false;
        }
        const distanceX = Math.abs(this.positionX-enemy.positionX);
        const distanceY = Math.abs(this.positionY-enemy.positionY);
        const totalDistance = distanceX + distanceY;
        if(totalDistance <= this.attackRange){
            return true;
        }else{
            this.messageModal.showMessage("C'est trop loin.\nTu ne peux pas effectuer une action.\n Ton tour est terminé.")
            return false;
        }
    }

    public attackEnemy(target:Unit){
        if(!this.availableActionWithTarget(target)){  
            return;
        }
        if(!this.isEnemy(target)){
            this.messageModal.showMessage("Ce n'est pas ton enemy")
            return  false;
        }
        const damage = this.attack - target.defense;
        if(damage > 0){
            target.hp -= damage;
            target.defense-=1;
            this.messageModal.showMessage(`${this.action} par ${this.name}!\n ${damage} damage sur ${target.name}!!!\nTon tour est terminé. `);
            if(target.hp<=0){
                target.alive = false;
                const event = new EventUnitDeath(target);
                document.dispatchEvent(event); 
            }
        }else{
            this.messageModal.showMessage(`${this.action} par ${this.name}!\n Mais aucun damage sur ${target.name}!!!\nTon tour est terminé. `);
        }    
    }

    public healAction(target:Unit){
        if(!this.availableActionWithTarget(target)){  
            return;
        }
        if(!target.alive){
            return;
        }
        if(this.name!="médecin"){
            return;
        }
        if (target.player !== this.player) {
            this.messageModal.showMessage("Impossible de soigner un ennemi.");
            return;
        }
        target.hp +=2;
        this.messageModal.showMessage(`Médecin a soigné ${target.name}. \nSon hp est ${target.hp} `);
    }

    public isMyturn():boolean{
        return this.player==Game.currentPlayer;
    }

    public isEnemy(target:Unit):boolean{
        return target.player != this.player; 
    }

    
}