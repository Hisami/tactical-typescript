import { CounterDog } from "./Components/CounterDog";
import { MessageModal } from "./Components/MessageModal";
import { MessageWindow } from "./Components/MessageWindow";
import { ResetButton } from "./Components/ResetButton";
import { Unit } from "./Unit";

export class Game{
    private gameBoard:(Unit|null)[][];
    static currentPlayer:string;
    div:HTMLDivElement|null = null;
    private currentAction: "move" | "attack" = "move"; 
    private selectedUnit:Unit|null = null;
    private messageWindow:MessageWindow;
    private messageModal:MessageModal;
    private counterDog:CounterDog;
    private gameCount:number;
    private gameOver:boolean;
    private winner:string | undefined;

    constructor(rows: number, columns: number) {
        this.gameOver = false;
        this.gameBoard = new Array(rows);
        this.gameCount = 1;
        for (let row = 0; row < rows; row++) {
            this.gameBoard[row] = new Array(columns).fill(null);
        }
        this.messageWindow = new MessageWindow();
        this.messageModal = new MessageModal();
        this.counterDog = new CounterDog();
        Game.currentPlayer = "player1";
        this.messageWindow.showMessage("Au tour du "+Game.currentPlayer+":Cliquez sur un personage de ton équipe pour déplacer une pièce.\nEnsuite, veuillez cliquer sur la case de destination.\nLa zone verte représente la portée de déplacement des unités.")
        this.messageModal.showMessage("Le jeu se déroule en 10 tours. À chaque tour, 1 move 1 attaque.Conditions de victoire  :  vaincre le général adverse,ou compter le nombre d'unités restes à la fin");
        this.counterDog.changeMessageandPosition(this.gameCount+"/10 "+"\n Au tour du "+ Game.currentPlayer,Game.currentPlayer);
        this.setInitialData();
        document.addEventListener('unit-death', (event:any) => {
            if(event.unit.name=="shogun"){
                this.gameOver=true;
                this.endGame();
                this.winner = event.unit.player=="player1"?"player2":"player1";
                this.messageWindow.showMessage(`Winner ${this.winner}!!!`);
                return;
            }
            this.render(); 
        });     
    }

    public setInitialData(){
        const unitData = [
            { player: "player1", type: "infantry", x: 0, y: 0 },
            { player: "player2", type: "infantry", x: 6, y: 0 },
            { player: "player1", type: "infantry", x: 0, y: 1 },
            { player: "player2", type: "infantry", x: 6, y: 1 },
            { player: "player1", type: "infantry", x: 0, y: 5 },
            { player: "player2", type: "infantry", x: 6, y: 5 },
            { player: "player1", type: "infantry", x: 0, y: 6 },
            { player: "player2", type: "infantry", x: 6, y: 6 },
            { player: "player1", type: "archer", x: 0, y: 2 },
            { player: "player2", type: "archer", x: 6, y: 4 },
            { player: "player1", type: "shogun", x: 0, y: 3 },
            { player: "player2", type: "shogun", x: 6, y: 3 },
            { player: "player1", type: "médecin", x: 0, y: 4 },
            { player: "player2", type: "médecin", x: 6, y: 2 }
        ];
        unitData.forEach(unitInfo => {
            let unit:Unit;
            switch(unitInfo.type){
                case "infantry":
                    unit = new Unit(unitInfo.player, unitInfo.type, 1, 1, "Attaquer avec une épée", 5, 2, unitInfo.x, unitInfo.y, 5,this.messageModal);
                    this.initialPosition(unit);
                    break;
                case "shogun":
                    unit = new Unit(unitInfo.player, unitInfo.type, 3, 2, "Attaquer avec une grande épée", 8, 3, unitInfo.x, unitInfo.y, 15,this.messageModal);
                    this.initialPosition(unit);
                    break;
                case "archer":
                    unit = new Unit(unitInfo.player, unitInfo.type, 1, 4, "Tirer à l'arc ", 4, 2, unitInfo.x, unitInfo.y, 5,this.messageModal);
                    this.initialPosition(unit);
                    break;
                case "médecin":
                    unit = new Unit(unitInfo.player, unitInfo.type, 2, 2, "Soigner", 2,2, unitInfo.x, unitInfo.y, 3,this.messageModal);
                    this.initialPosition(unit);
                    break;
                default:
            }
            
        });
    }
    public initialPosition(unit:Unit){
        this.gameBoard[unit.positionX][unit.positionY]= unit;
    }

    public setUnit(unit: Unit, destinationX: number, destinationY: number): void {
        if (!this.isEmpty(destinationX, destinationY)) {
            if(unit.player != Game.currentPlayer){
            this.showModal("Au début de votre tour, choisissez une de vos cartes alliées, déplacez-la, puis attaquez avec cette carte.");  
            }
            this.showModal("Mouvement invalide. Veuillez sélectionner une destination valide.");
            return;
        }
        if (!unit.availableMove(destinationX, destinationY)) {
            this.showModal("Vous ne pouvez pas vous déplacer ici. Veuillez sélectionner une destination valide.");
            return; 
        }
        this.gameBoard[unit.positionX][unit.positionY] = null;
        this.gameBoard[destinationX][destinationY] = unit;
        unit.positionX = destinationX;
        unit.positionY = destinationY;
        this.currentAction = "attack";
        this.messageWindow.showMessage("Au tour du "+Game.currentPlayer+":Cliquez sur un enemy pour l'attaquer.\n Dans le cas d'un médecin, vous pouvez sélectionner un allié et guérir 2 points de vie.\nLa zone rouge représente la portée de déplacement des unités.")
    }

    public getBoard():(Unit|null)[][]{
        return this.gameBoard;
    }

    public isEmpty(row:number, column:number):boolean{
        return this.gameBoard[row][column]==null;
    }

    public switchPlayer(){
        if (this.gameOver) {
            return;
        }
        if(Game.currentPlayer=="player1"){
            Game.currentPlayer = "player2";
        }else{
            Game.currentPlayer = "player1";
            this.gameCount++;
            if (this.gameCount > 10) {
                this.messageModal.showMessage("C'est la fin du jeux!");
                this.gameOver=true;
                this.endGame();
                return;
            }
        }
        this.currentAction="move";
        this.selectedUnit=null;
        this.messageWindow.showMessage("Au tour du "+Game.currentPlayer+" :Cliquez sur un personage pour se déplacer.\n Ensuite, veuillez cliquer sur la case de destination.\nLa zone verte représente la portée de déplacement des unités.");
        this.counterDog.changeMessageandPosition(this.gameCount+"/10tours "+"\n Au tour du "+ Game.currentPlayer,Game.currentPlayer)
    }

    public showMessage(message: string) {
        this.messageWindow.showMessage(message);
    }

    public showModal(message:string){
        this.messageModal.showMessage(message);
    }
    public render(){
        if(this.div == null) {
            this.div = document.createElement('div')
        }
        this.div.innerHTML = '';
        if(!this.gameOver){
            for(let rowIndex=0; rowIndex < this.gameBoard.length; rowIndex++){
                const row = this.gameBoard[rowIndex];
                const rowElement = document.createElement('div');
                rowElement.className = "row";
                let previouslySelectedCell:HTMLElement|null=null;
                for(let cellIndex=0; cellIndex < row.length; cellIndex++){
                    const cell = row[cellIndex];
                    const cellElement = document.createElement('div');
                    //call endGame if shogun is dead
                    if(cell?.name=="shogun" && !cell.alive){
                        this.messageModal.showMessage(`${cell.player} LOSE!`)
                        this.winner = cell.player == "player1" ? "player2" : "player1";
                        this.endGame();
                        return;
                    }
                    cellElement.className = "cell";
                    this.highlightColorMethod(cellElement, rowIndex, cellIndex);

                    cellElement.addEventListener('click',()=>{
                        if(this.currentAction=="move"){
                            previouslySelectedCell = this.moveMethod(cell, cellElement, previouslySelectedCell, rowIndex, cellIndex);
                        }else{
                            //this.currentAction =="attack"
                            this.actionMethod(cell);
                            this.switchPlayer();
                            this.render();
                        }
                    }) 
                    this.addImg(cell, cellElement);
                rowElement.append(cellElement);    
                }
                this.div.append(rowElement);
                const button = new ResetButton().getButton()
                this.div.append(button);
            } 
        }else{
            //this.GameOver ==true;
            const img = document.createElement('img');
            img.setAttribute("src","samurai.png");
            img.setAttribute("height", "500");
            img.setAttribute("width", "500");
            const button = new ResetButton().getButton()
            this.div.append(img);
            this.div.append(button);
        }
        return this.div;
    }

    private highlightColorMethod(cellElement: HTMLDivElement, rowIndex: number, cellIndex: number) {
        if (this.currentAction == "move") {
            cellElement.classList.remove('action-range');
            if (this.selectedUnit != null && this.selectedUnit.availableMove(rowIndex, cellIndex)) {
                cellElement.classList.add('move-range');
            }
        } else {
            cellElement.classList.remove('move-range');
            if (this.selectedUnit != null && this.selectedUnit.availableAction(rowIndex, cellIndex)) {
                cellElement.classList.add('action-range');
            }
        }
    }

    private moveMethod(cell: Unit | null, cellElement: HTMLDivElement, previouslySelectedCell: HTMLElement | null, rowIndex: number, cellIndex: number) {
        if (this.selectedUnit == null && cell != null) {
            this.selectedUnit = this.select(cell, this.selectedUnit, cellElement);
            previouslySelectedCell = cellElement;
            this.render();
        } else if (this.selectedUnit != null) {
            if (cell == this.selectedUnit) {
                this.selectedUnit = null;
                cellElement.classList.remove('selected-cell');
            } else if (cell?.player == this.selectedUnit.player) {
                this.selectedUnit = cell;
                cellElement.classList.add('selected-cell');
                if (previouslySelectedCell != null) {
                    previouslySelectedCell.classList.remove('selected-cell');
                }
                previouslySelectedCell = cellElement;
            }
            this.selectedUnit = this.move(this.selectedUnit, rowIndex, cellIndex);
        }
        return previouslySelectedCell;
    }

    private actionMethod(cell: Unit | null) {
        if (cell != null && this.selectedUnit != null) {
            if (this.selectedUnit.isEnemy(cell)) {
                this.selectedUnit.attackEnemy(cell);
            } else if (this.selectedUnit.name == "médecin") {
                this.selectedUnit.healAction(cell);
            } else {
                this.messageModal.showMessage("Tu ne peux pas attaquer ton propre camp.\n Votre tour est terminé.");
            }
        } else {
            this.messageModal.showMessage("Échec de l'attaque.\nTu ne peux attaquer personne.\n Votre tour est terminé.");
        }
    }

    private addImg(cell: Unit | null, cellElement: HTMLDivElement) {
        if (cell != null) {
            if (cell.alive) {
                cellElement.textContent = cell.name;
                const img = document.createElement('img');
                img.setAttribute("src", cell.name + ".png");
                img.setAttribute("height", "42");
                img.setAttribute("width", "42");
                cellElement.appendChild(img);
                const p = document.createElement('p');
                p.textContent="HP: "+cell.hp;
                p.style.fontSize="12px";
                p.style.marginTop="0px"
                cellElement.appendChild(p);
                if (cell.player === "player1") {
                    cellElement.classList.add('player1-cell');
                } else if (cell.player === "player2") {
                    cellElement.classList.add('player2-cell');
                }
            } else {
            }
        }
    }

    private move(selectedUnit: Unit | null, rowIndex: number, cellIndex: number) {
        this.setUnit(selectedUnit as Unit, rowIndex, cellIndex);
        this.render();
        return selectedUnit;
    }

    private select(cell: Unit, selectedUnit: Unit | null, cellElement: HTMLDivElement) {
        if (cell.player == Game.currentPlayer) {
            selectedUnit = cell;
            cellElement.classList.add('selected-cell');
        }
        return selectedUnit;
    }

    public getAliveUnitCount(player: string): number {
        let count = 0;
        for (let row = 0; row < this.gameBoard.length; row++) {
            for (let column = 0; column < this.gameBoard[row].length; column++) {
                const unit = this.gameBoard[row][column];
                if (unit && unit.player === player && unit.alive) {
                    count++;
                }
            }
        }
        return count;
    }

    public endGame(){
        this.counterDog.hide();
        if(this.gameCount>10){
            const player1Alive = this.getAliveUnitCount("player1");
            const player2Alive = this.getAliveUnitCount("player2");
            const difference = player1Alive-player2Alive;
            if(difference >0){
                this.winner = "player1"
                this.messageWindow.showMessage( this.winner + " gagné!")
            }else if(difference <0){
                this.winner = "player2"
                this.messageWindow.showMessage( this.winner + " gagné!")
            }else{
                this.winner = "";
                this.messageWindow.showMessage("match null!!!");
            }
        }
        this.render();
    }
    
}